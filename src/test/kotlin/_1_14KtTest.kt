import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{
    @Test
    fun normalBill(){
        assertEquals( "15,00" ,billDivider(8,120.0))
    }
    @Test
    fun billWithLotsOfDecimals(){
        assertEquals( "17,02" ,billDivider(5,85.123456789))
    }
    @Test
    fun bigDinner(){
        assertEquals( "23,23" ,billDivider(85465, 1985154.26154))
    }
}