import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{
    @Test
    fun multiplicatioBy0(){
        assertEquals(0, multiplyBy2(0))
    }
    @Test
    fun multiplicationNegativeNumber(){
        assertEquals(-10, multiplyBy2(-5))
    }
    @Test
    fun mulitplicationBigNumber(){
        assertEquals(169024, multiplyBy2(84512))
    }

}

