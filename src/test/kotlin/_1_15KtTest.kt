import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{
    @Test
    fun sumOfASecond(){
        assertEquals(8,addSecond(7))
    }
    @Test
    fun changeOfSecondToMinute(){
        assertEquals(0,addSecond(59))
    }
    @Test
    fun changeOfMinuteTo1Second(){
        assertEquals(6, addSecond(65))
    }

}