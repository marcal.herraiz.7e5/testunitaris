import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{
    @Test
    fun pairOfStudents(){
        assertEquals( 9,sumOfPupitres(4,8,6))
    }
    @Test
    fun unpairOfStudents(){
        assertEquals(10 , sumOfPupitres(8,5,6))
    }
    @Test
    fun hugeClassOfStudents(){
        assertEquals(4775674 , sumOfPupitres(8545,21345,9521457))
    }
}