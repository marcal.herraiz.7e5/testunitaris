import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{
    @Test
    fun calculateNormalVolum(){
        assertEquals( "504,000",calculateAirVolum(8.0,9.0,7.0))
    }
    @Test
    fun calculateVolumWithDecimals(){
        assertEquals( "598,561",calculateAirVolum(8.5,9.84,7.1564))
    }
    @Test
    fun calculateBigVolum(){
        assertEquals( "19920736609784954000000,000",calculateAirVolum(84615.0,32451599.0,7254741356.0))
    }
}