import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{
    @Test
    fun multiplicationOfNegativeNumbers(){
        assertEquals(400, multiply2Numbers(-40, -10))
    }
    @Test
    fun multiplicationOfOneNegativeNumber(){
        assertEquals(-180, multiply2Numbers(-6 ,30))
    }
    @Test
    fun multiplicationOfBigNumbers(){
        assertEquals( 1057768980 ,multiply2Numbers(12345, 85684))
    }
}