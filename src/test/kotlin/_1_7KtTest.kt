import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{
    @Test
    fun nextPositiveNumber(){
        assertEquals(5,nextNumber(4))
    }
    @Test
    fun nextNegativeNumber(){
        assertEquals(-5, nextNumber(-6))
    }
    @Test
    fun nextNumberIs0(){
        assertEquals(0, nextNumber(-1))
    }
}