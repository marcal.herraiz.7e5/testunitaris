import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{
    @Test
    fun allPositiveNumbers(){
        assertEquals(13 , crazyOperation(5,8,9,4))
    }
    @Test
    fun allNegativeNumbers(){
        assertEquals(2678, crazyOperation(-58,-45,-26,-34))
    }
    @Test
    fun multiplyBy0(){
        assertEquals(0, crazyOperation(8,5,0,9))
    }
}