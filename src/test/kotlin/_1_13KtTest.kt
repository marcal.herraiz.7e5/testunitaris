import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun normalTemperatureRaise(){
        assertEquals(29.0 ,raiseOfTemperature(24.0, 5.0))
    }
    @Test
    fun temperatureRaiseWithDecimals(){
        assertEquals(15.1438 ,raiseOfTemperature(12.1598, 2.984))
    }
    @Test
    fun hugeTemperatureRaise(){
        assertEquals(90798.0 ,raiseOfTemperature(45647.0, 45151.0))
    }
}