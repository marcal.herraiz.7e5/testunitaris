import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{
    @Test
    fun discountOf100(){
        assertEquals("98,00", calculateDiscount(100.0,2.0))
    }
    @Test
    fun discountOfBigNumber(){
        assertEquals("71,26", calculateDiscount(15961.0,4587.0))
    }
    @Test
    fun discountWithDecimals(){
        assertEquals("21,39", calculateDiscount(8745.15,6874.85))
    }
}