import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{
    @Test
    fun positiveNumber(){
        assertEquals(54.0, intToDouble(54))
    }
    @Test
    fun negativeNumber(){
        assertEquals(-54.0, intToDouble(-54))
    }
    @Test
    fun hugeNumber(){
        assertEquals(98754324.0, intToDouble(98754324))
    }
}