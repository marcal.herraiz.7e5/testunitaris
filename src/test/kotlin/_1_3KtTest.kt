import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3KtTest{
    @Test
    fun sumOfNegativeNumbers(){
        assertEquals(-50, sumOf2Numbers(-40, -10))
    }
    @Test
    fun sumOfOneNegativeNumber(){
        assertEquals(24, sumOf2Numbers(-6 ,30))
    }
    @Test
    fun sumOfBigNumbers(){
        assertEquals( 869191099 ,sumOf2Numbers(12345678, 856845421))
    }
}