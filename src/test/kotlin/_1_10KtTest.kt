import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun calculateNormalArea(){
        assertEquals("5674,50" ,calculateArea(85.0))
    }
    @Test
    fun calculateAreaWithDecimals(){
        assertEquals( "21423,69",calculateArea(165.159))
    }
    @Test
    fun calculateAreaBigNumber(){
        assertEquals( "51982454094285,84" ,calculateArea(8135485.0))
    }
}