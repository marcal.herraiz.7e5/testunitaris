import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{
    @Test
    fun doubleOfPositiveNumber(){
        assertEquals( 118.0,doubleNumber(59.0))
    }
    @Test
    fun doubleOfNegativeNumber(){
        assertEquals( -30.0 ,doubleNumber(-15.0))
    }
    @Test
    fun doubleOfDoubleNumber(){
        assertEquals( 10.4 ,doubleNumber(5.2))
    }
}