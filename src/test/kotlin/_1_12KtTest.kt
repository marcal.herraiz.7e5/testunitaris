import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{
    @Test
    fun normalCelsius(){
        assertEquals("59,00", changeCelsiusToFahrenheit(15.0))
    }
    @Test
    fun negativeCelsius(){
        assertEquals( "14,00" ,changeCelsiusToFahrenheit(-10.0))
    }
    @Test
    fun celsiusWithDecimals(){
        assertEquals( "74,48",changeCelsiusToFahrenheit(23.598))
    }
}