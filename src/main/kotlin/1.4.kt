import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Calcula l’àrea
*/

fun main() {
    val sc = Scanner(System.`in`)
    val number1 = sc.nextInt()
    val number2 = sc.nextInt()
    print(multiply2Numbers(number1, number2))
}

fun multiply2Numbers(number1: Int, number2: Int): Int {
    return number1 * number2
}
