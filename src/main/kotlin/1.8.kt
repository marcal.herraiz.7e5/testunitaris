import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Dobla el decimal
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val double = sc.nextDouble()
    print(doubleNumber(double))
}

fun doubleNumber(number: Double) : Double{
    return number*2
}