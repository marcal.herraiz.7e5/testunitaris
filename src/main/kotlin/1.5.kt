import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Operació boja
*/

fun main() {
    val sc = Scanner(System.`in`)
    val a = sc.nextInt()
    val b = sc.nextInt()
    val c = sc.nextInt()
    val d = sc.nextInt()
    print(crazyOperation(a,b,c,d))
}

fun crazyOperation(a: Int, b: Int, c: Int, d: Int): Int {
    return (a + b) * (c % d)
}
