import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Quina temperatura fa?
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val temp = sc.nextDouble()
    val aug = sc.nextDouble()
    println("La temperatura actual és ${raiseOfTemperature(temp,aug)}°")

}

fun raiseOfTemperature(temp: Double, aug: Double): Double {
    return temp+aug
}
