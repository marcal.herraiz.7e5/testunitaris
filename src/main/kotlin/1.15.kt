import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/21
* TITLE: Afegeix un segon
*/

fun main() {
    val sc = Scanner(System.`in`)
    val int = sc.nextInt()
    println(addSecond(int))

}

fun addSecond(int: Int): Int {
    return (int+1)%60
}
