import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Calcula el descompte
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val double1 = sc.nextDouble()
    val double2 = sc.nextDouble()
    println(calculateDiscount(double1,double2))
}

fun calculateDiscount(number1 : Double, number2: Double): String {
    val difference = number1 - number2
    return ("%.2f".format((difference/number1)*100))
}