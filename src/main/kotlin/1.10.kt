import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Quina és la mida de la meva pizza?
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val double = sc.nextDouble()
    print(calculateArea(double))
}

fun calculateArea(number1 : Double) : String{
    return "%.2f".format((number1/2)*(number1/2)*Math.PI)
}