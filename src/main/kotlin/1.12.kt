import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: De Celsius a Fahrenheit
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val double = sc.nextDouble()
    println(changeCelsiusToFahrenheit(double))
}

fun changeCelsiusToFahrenheit(double: Double): String {
    return "%.2f".format(double*1.8+32)
}
