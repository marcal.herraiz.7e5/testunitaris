import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Pupitres
*/

fun main() {
    val sc = Scanner(System.`in`)
    val int1 = sc.nextInt()
    val int2 = sc.nextInt()
    val int3 = sc.nextInt()
    print(sumOfPupitres(int1,int2,int3))
}

fun sumOfPupitres(int1: Int, int2: Int, int3: Int): Int {
    return ((int1+int2+int3)/2)+((int1+int2+int3)%2)
}
