import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Suma de dos nombres enters
*/

fun main() {
    val sc = Scanner(System.`in`)
    val number1 = sc.nextInt()
    val number2 = sc.nextInt()
    print(sumOf2Numbers(number1, number2))

}

fun sumOf2Numbers(number1: Int, number2: Int): Int {
    return number1+number2
}
