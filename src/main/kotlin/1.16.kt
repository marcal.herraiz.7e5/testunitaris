import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/21
* TITLE: Transforma l’enter
*/

fun main() {
    val sc = Scanner(System.`in`)
    val int = sc.nextInt()
    println(intToDouble(int))
}

fun intToDouble(int: Int): Double {
    return int.toDouble()
}
