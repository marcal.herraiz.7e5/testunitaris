import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Número següent
*/

fun main() {
    val sc = Scanner(System.`in`)
    val int = sc.nextInt()
    println("Després ve el ${nextNumber(int)}")
}

fun nextNumber(int: Int): Int {
    return int+1
}
