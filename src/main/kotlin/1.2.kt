import java.util.Scanner

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Dobla l’enter
*/

fun main() {
    val sc = Scanner(System.`in`)
    val number = sc.nextInt()
    print(multiplyBy2(number))
}

fun multiplyBy2(number: Int): Int {
    return number*2
}

