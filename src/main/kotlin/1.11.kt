import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Calculadora de volum d’aire
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val a = sc.nextDouble()
    val b = sc.nextDouble()
    val c = sc.nextDouble()

    println(calculateAirVolum(a,b,c))
}

fun calculateAirVolum(a: Double, b: Double, c: Double): String {
    return "%.3f".format(a*b*c)
}
