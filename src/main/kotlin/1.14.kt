import java.util.*

/*
* AUTHOR: Marçal Herraiz Prat
* DATE: 2022/09/19
* TITLE: Divisor de compte
*/

fun main() {
    val sc = Scanner(System.`in`).useLocale(Locale.UK)
    val int = sc.nextInt()
    val double = sc.nextDouble()
    println("${billDivider(int,double)}€")
}

fun billDivider(int: Int ,double: Double): String {
    return ("%.2f".format(double/int))
}
